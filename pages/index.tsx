import Head from 'next/head'
import firebase from 'firebase/app'
import 'firebase/storage'
import { useState, ChangeEvent } from 'react'

const config = {
  apiKey: process.env.FIREBASE_KEY,
  projectId: 'lamers-no',
  storageBucket: 'lamers-no.appspot.com',
  childPath: '/',
}

!firebase.apps.length ? firebase.initializeApp(config) : firebase.app()

export default function Home() {
  const [opacity, setOpacity] = useState(1)
  const [image, setImage] = useState(
    'https://firebasestorage.googleapis.com/v0/b/lamers-no.appspot.com/o/lamers?alt=media'
  )

  const firebaseUploader = ({ target: { files } }: ChangeEvent<HTMLInputElement>) => {
    if (files[0].size > 15 * 1024 * 1024) return alert('Bummer! There is a 15 MB file limit.')

    const storageReference = firebase.storage().ref()
    const reference = storageReference.child(`lamers`)
    const uploadTask = reference.put(files[0])
    setOpacity(0)

    uploadTask.on(
      'state_changed',
      () => {},
      (error) => {
        alert(error)
      },
      async () => {
        setImage(await uploadTask.snapshot.ref.getDownloadURL())
      }
    )
  }

  return (
    <div className='container'>
      <Head>
        <title>Lamers NO! - Click to change picture</title>
      </Head>
      <main>
        <label htmlFor='image-upload'>
          <img
            src={image}
            title='Click to change to your own sweet-ass picture'
            alt='A picture that can be replaced by clicking it and uploading a new one'
            style={{ opacity }}
            onLoad={() => setOpacity(1)}
          />
        </label>
        <input
          id='image-upload'
          type='file'
          onChange={firebaseUploader}
          accept='.gif,.jpg,.jpeg,.png,image/gif, image/jpeg, image/png'
        />
      </main>

      <style jsx global>
        {`
          body {
            margin: 0;
            background: black;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
          }

          main {
            position: relative;
          }

          label {
            display: block;
            overflow: hidden;
            max-height: 100vh;
          }

          img {
            object-fit: contain;
            cursor: pointer;
            transition: opacity 0.25s;
            max-width: 100vw;
            max-height: 100vh;
          }

          input {
            display: none;
          }
        `}
      </style>
      <script async defer src='https://scripts.simpleanalyticscdn.com/latest.js'></script>
      <noscript>
        <img src='https://queue.simpleanalyticscdn.com/noscript.gif' alt='' />
      </noscript>
    </div>
  )
}
